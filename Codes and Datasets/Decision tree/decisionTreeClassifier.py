#part 01: import libraries
import csv
import copy

#part 02: Extract data from source(csv file)
#create six lists to store input features in the data source(a csv file)
L_classification = list()
L_feature_1 = list()
L_feature_2 = list()
L_feature_3 = list()
L_feature_4 = list()
L_feature_5 = list()

#enable the access to data source(a csv file)
csvFile = open('C:\\Users\\Cheny\\Desktop\\ML_project_Dataset\\training.csv', 'r')
reader = csv.reader(csvFile)
#copy and paste the input features from the file reader to existing six lists
for record in reader:
	if reader.line_num == 1:
		continue
	L_classification.append(record[0])
	L_feature_1.append(record[1])
	L_feature_2.append(record[2])
	L_feature_3.append(record[3])
	L_feature_4.append(record[4])
	L_feature_5.append(record[5])

#after all input features have been extracted, the file reader should be closed to return I/O subsystem control to operating system
csvFile.close()

#because the extracted data is not float type, convert them to float type and create five new lists to store them
L_feature_1_float = [float(i) for i in L_feature_1]
L_feature_2_float = [float(i) for i in L_feature_2]
L_feature_3_float = [float(i) for i in L_feature_3]
L_feature_4_float = [float(i) for i in L_feature_4]
L_feature_5_float = [float(i) for i in L_feature_5]

#calculate the total number of training examples and store it
total_number = len(L_classification)

#part 3: Because each node in decision tree process one feature, this part generate all possible orders of features
#create 9 variables
order_features_list = list() #this list is used to store all possible orders of features
temp_1 = list() #this list stores all candidate features before the 1st choice is made in order to restore this status in the future
temp_2 = list() #this list stores all candidate features before the 2nd choice is made in order to restore this status in the future
temp_3 = list() #this list stores all candidate features before the 3rd choice is made in order to restore this status in the future
temp_4 = list() #this list stores all candidate features before the 4th choice is made in order to restore this status in the future
order_1 = 0 #this variable stores the choice of features' order after the 1st choice is made in order to restore this status in the future
order_2 = 0 #this variable stores the choice of features' order after the 2nd choice is made in order to restore this status in the future
order_3 = 0 #this variable stores the choice of features' order after the 3rd choice is made in order to restore this status in the future
order_4 = 0 #this variable stores the choice of features' order after the 4th choice is made in order to restore this status in the future
for i in range(5):
        #this code segment choose the 1st order of features
        order = 0 #restore the status to previous one(before the 1st order is chosen)
        order += (i+1)*10000
        order_1 = order
        temp = [1,2,3,4,5] #restore the status to previous one(before the 1st order is chosen)
        temp_1 = copy.copy(temp)
        temp.remove(i+1)
        for j in range(4):
                #this code segment chooses the 2nd order of features
                order += temp[j]*1000
                order_2 = order
                temp_2 = copy.copy(temp)
                temp.remove(temp[j])
                for k in range(3):
                        #this code segment chooses the 3rd order of features
                        order += temp[k]*100
                        order_3 = order
                        temp_3 = copy.copy(temp)
                        temp.remove(temp[k])
                        for m in range(2):
                                #this code segment chooses the 4th order of features
                                order += temp[m]*10
                                order_4 = order
                                temp_4 = copy.copy(temp)
                                temp.remove(temp[m])
                                for n in range(1):
                                        #this code segment chooses the 5th order of features
                                        order += temp[0]
                                        order_features_list.append(order)
                                        temp = copy.copy(temp_4) #restore the status to previous one(before the 5th order is chosen)
                                        order = order_4 #restore the status to previous one(before the 5th order is chosen)
                                if m == 1:
                                        temp = copy.copy(temp_3) #restore the status to previous one(before the 4th order is chosen)
                                order = order_3 #restore the status to previous one(before the 4th order is chosen)
                        if k == 2:
                                temp = copy.copy(temp_2) #restore the status to previous one(before the 3rd order is chosen)
                        order = order_2 #restore the status to previous one(before the 3rd order is chosen)
                if j == 3:
                        temp = copy.copy(temp_1) #restore the status to previous one(before the 2nd order is chosen)
                order = order_1 #restore the status to previous one(before the 2nd order is chosen)

#test code segment
#print(order_features_list)

#test code segment
#print(len(order_features_list))

#part 4: In part 3, all possible orders of features have been generated. In this part, generate all candidate decision trees according to these orders of features.
paths = list() #this list stores all candidate decision trees' structure
#initialize this list
for i in range(120):
        paths.append(0)

results = list() #this list stores how many training examples cannot be identified by each candidate decision tree
#initialize this list
for i in range(120):
        results.append(-1)

for ii in range(120): #each iteration in this loop is corresponded to a unique order of features
        
        #these 4 variables store the result of statistics for the values of 1st chosen feature which is extracted from a specific order of features among all possible orders of features
        range_w_min_1 = 0.0  
        range_w_max_1 = 0.0
        range_u_min_1 = 0.0
        range_u_max_1 = 0.0
        #at the beginning of each iteration, these 4 variables must be reset

        #these 4 variables store the result of statistics for the values of 2nd chosen feature which is extracted from a specific order of features among all possible orders of features
        range_w_min_2 = 0.0
        range_w_max_2 = 0.0
        range_u_min_2 = 0.0
        range_u_max_2 = 0.0
        #at the beginning of each iteration, these 4 variables must be reset
        
        #these 4 variables store the result of statistics for the values of 3rd chosen feature which is extracted from a specific order of features among all possible orders of features
        range_w_min_3 = 0.0
        range_w_max_3 = 0.0
        range_u_min_3 = 0.0
        range_u_max_3 = 0.0
        #at the beginning of each iteration, these 4 variables must be reset

        #these 4 variables store the result of statistics for the values of 4th chosen feature which is extracted from a specific order of features among all possible orders of features
        range_w_min_4 = 0.0
        range_w_max_4 = 0.0
        range_u_min_4 = 0.0
        range_u_max_4 = 0.0
        #at the beginning of each iteration, these 4 variables must be reset

        #these 4 variables store the result of statistics for the values of 5th chosen feature which is extracted from a specific order of features among all possible orders of features
        range_w_min_5 = 0.0
        range_w_max_5 = 0.0
        range_u_min_5 = 0.0
        range_u_max_5 = 0.0
        #at the beginning of each iteration, these 4 variables must be reset

        #these 6 lists store all training examples which cannot be identified by the 1st node
        L_classification_a1 = list()
        L_feature_1_a1 = list()
        L_feature_2_a1 = list()
        L_feature_3_a1 = list()
        L_feature_4_a1 = list()
        L_feature_5_a1 = list()
        #at the beginning of each iteration, these 6 lists must be reset

        #these 6 lists store all training examples which cannot be identified by the 1st node
        L_classification_a2 = list()
        L_feature_1_a2 = list()
        L_feature_2_a2 = list()
        L_feature_3_a2 = list()
        L_feature_4_a2 = list()
        L_feature_5_a2 = list()
        #at the beginning of each iteration, these 6 lists must be reset

        #these 6 lists store all training examples which cannot be identified by the 1st node
        L_classification_a3 = list()
        L_feature_1_a3 = list()
        L_feature_2_a3 = list()
        L_feature_3_a3 = list()
        L_feature_4_a3 = list()
        L_feature_5_a3 = list()
        #at the beginning of each iteration, these 6 lists must be reset

        #these 6 lists store all training examples which cannot be identified by the 1st node
        L_classification_a4 = list()
        L_feature_1_a4 = list()
        L_feature_2_a4 = list()
        L_feature_3_a4 = list()
        L_feature_4_a4 = list()
        L_feature_5_a4 = list()
        #at the beginning of each iteration, these 6 lists must be reset

        #these 6 lists store all training examples which cannot be identified by the 1st node
        L_classification_a5 = list()
        L_feature_1_a5 = list()
        L_feature_2_a5 = list()
        L_feature_3_a5 = list()
        L_feature_4_a5 = list()
        L_feature_5_a5 = list()
        #at the beginning of each iteration, these 6 lists must be reset

        #extract order of features and store them in 5 variables
        order = order_features_list[ii]
        index_feature_1 = order//10000
        index_feature_2 = (order//1000)%10
        index_feature_3 = (order//100)%10
        index_feature_4 = (order//10)%10
        index_feature_5 = order%10

        #test code segment
        #print(index_feature_1*10000 + index_feature_2*1000 + index_feature_3*100 + index_feature_4*10 + index_feature_5)

        #if the 1st node deals with the 1st feature, compute the ranges of this feature's values for different classes
        if index_feature_1 == 1:
                range_w_min_1 = L_feature_1_float[0]
                range_w_max_1 = L_feature_1_float[0]
                range_u_min_1 = L_feature_1_float[0]
                range_u_max_1 = L_feature_1_float[0]
                for i in range(len(L_classification)):
                        if L_classification[i] == 'w':
                                if L_feature_1_float[i] < range_w_min_1:
                                        range_w_min_1 = L_feature_1_float[i]
                                if L_feature_1_float[i] > range_w_max_1:
                                        range_w_max_1 = L_feature_1_float[i]
                        if L_classification[i] == 'u':
                                if L_feature_1_float[i] < range_u_min_1:
                                        range_u_min_1 = L_feature_1_float[i]
                                if L_feature_1_float[i] > range_u_max_1:
                                        range_u_max_1 = L_feature_1_float[i]
                
        #if the 1st node deals with the 2nd feature, compute the ranges of this feature's values for different classes
        if index_feature_1 == 2:
                range_w_min_1 = L_feature_2_float[0]
                range_w_max_1 = L_feature_2_float[0]
                range_u_min_1 = L_feature_2_float[0]
                range_u_max_1 = L_feature_2_float[0]
                for i in range(len(L_classification)):
                        if L_classification[i] == 'w':
                                if L_feature_2_float[i] < range_w_min_1:
                                        range_w_min_1 = L_feature_2_float[i]
                                if L_feature_2_float[i] > range_w_max_1:
                                        range_w_max_1 = L_feature_2_float[i]
                        if L_classification[i] == 'u':
                                if L_feature_2_float[i] < range_u_min_1:
                                        range_u_min_1 = L_feature_2_float[i]
                                if L_feature_2_float[i] > range_u_max_1:
                                        range_u_max_1 = L_feature_2_float[i]
                
        #if the 1st node deals with the 3rd feature, compute the ranges of this feature's values for different classes
        if index_feature_1 == 3:
                range_w_min_1 = L_feature_3_float[0]
                range_w_max_1 = L_feature_3_float[0]
                range_u_min_1 = L_feature_3_float[0]
                range_u_max_1 = L_feature_3_float[0]
                for i in range(len(L_classification)):
                        if L_classification[i] == 'w':
                                if L_feature_3_float[i] < range_w_min_1:
                                        range_w_min_1 = L_feature_3_float[i]
                                if L_feature_3_float[i] > range_w_max_1:
                                        range_w_max_1 = L_feature_3_float[i]
                        if L_classification[i] == 'u':
                                if L_feature_3_float[i] < range_u_min_1:
                                        range_u_min_1 = L_feature_3_float[i]
                                if L_feature_3_float[i] > range_u_max_1:
                                        range_u_max_1 = L_feature_3_float[i]
                
        #if the 1st node deals with the 4th feature, compute the ranges of this feature's values for different classes
        if index_feature_1 == 4:
                range_w_min_1 = L_feature_4_float[0]
                range_w_max_1 = L_feature_4_float[0]
                range_u_min_1 = L_feature_4_float[0]
                range_u_max_1 = L_feature_4_float[0]
                for i in range(len(L_classification)):
                        if L_classification[i] == 'w':
                                if L_feature_4_float[i] < range_w_min_1:
                                        range_w_min_1 = L_feature_4_float[i]
                                if L_feature_4_float[i] > range_w_max_1:
                                        range_w_max_1 = L_feature_4_float[i]
                        if L_classification[i] == 'u':
                                if L_feature_4_float[i] < range_u_min_1:
                                        range_u_min_1 = L_feature_4_float[i]
                                if L_feature_4_float[i] > range_u_max_1:
                                        range_u_max_1 = L_feature_4_float[i]
                
        #if the 1st node deals with the 5th feature, compute the ranges of this feature's values for different classes
        if index_feature_1 == 5:
                range_w_min_1 = L_feature_5_float[0]
                range_w_max_1 = L_feature_5_float[0]
                range_u_min_1 = L_feature_5_float[0]
                range_u_max_1 = L_feature_5_float[0]
                for i in range(len(L_classification)):
                        if L_classification[i] == 'w':
                                if L_feature_5_float[i] < range_w_min_1:
                                        range_w_min_1 = L_feature_5_float[i]
                                if L_feature_5_float[i] > range_w_max_1:
                                        range_w_max_1 = L_feature_5_float[i]
                        if L_classification[i] == 'u':
                                if L_feature_5_float[i] < range_u_min_1:
                                        range_u_min_1 = L_feature_5_float[i]
                                if L_feature_5_float[i] > range_u_max_1:
                                        range_u_max_1 = L_feature_5_float[i]
                
        #if the 1st node can identify all training examples, work done. Disable the following procedures.
        if range_w_max_1 < range_u_min_1:
                index_feature_2 = 0
                index_feature_3 = 0
                index_feature_4 = 0
                index_feature_5 = 0
                paths[ii] = index_feature_1
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 1st node can identify all training examples, work done. Disable the following procedures.
        elif range_u_max_1 < range_w_min_1:
                index_feature_2 = 0
                index_feature_3 = 0
                index_feature_4 = 0
                index_feature_5 = 0
                paths[ii] = index_feature_1
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 1st node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_1 <= range_u_min_1 and range_u_min_1 < range_u_max_1 and range_u_max_1 <= range_w_max_1:
                paths[ii] = index_feature_1
                for j in range(len(L_classification)):
                        if index_feature_1 == 1: #if the first order of features is 1, search by feature 1
                                if range_u_min_1 <= L_feature_1_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 2: #if the first order of features is 2, search by feature 2
                                if range_u_min_1 <= L_feature_2_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 3: #if the first order of features is 3, search by feature 3
                                if range_u_min_1 <= L_feature_3_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 4: #if the first order of features is 4, search by feature 4
                                if range_u_min_1 <= L_feature_4_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 5: #if the first order of features is 5, search by feature 5
                                if range_u_min_1 <= L_feature_5_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])

        #if the 1st node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_1 <= range_w_min_1 and range_w_min_1 < range_w_max_1 and range_w_max_1 <= range_u_max_1: #intersection is w
                paths[ii] = index_feature_1
                for j in range(len(L_classification)):
                        if index_feature_1 == 1: #if the first order of features is 1, search by feature 1
                                if range_w_min_1 <= L_feature_1_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 2: #if the first order of features is 2, search by feature 2
                                if range_w_min_1 <= L_feature_2_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 3: #if the first order of features is 3, search by feature 3
                                if range_w_min_1 <= L_feature_3_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 4: #if the first order of features is 4, search by feature 4
                                if range_w_min_1 <= L_feature_4_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 5: #if the first order of features is 5, search by feature 5
                                if range_w_min_1 <= L_feature_5_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        

        #if the 1st node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_1 < range_w_max_1: 
                paths[ii] = index_feature_1
                for j in range(len(L_classification)):
                        if index_feature_1 == 1: #if the first order of features is 1, search by feature 1
                                if range_u_min_1 <= L_feature_1_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 2: #if the first order of features is 2, search by feature 2
                                if range_u_min_1 <= L_feature_2_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 3: #if the first order of features is 3, search by feature 3
                                if range_u_min_1 <= L_feature_3_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 4: #if the first order of features is 4, search by feature 4
                                if range_u_min_1 <= L_feature_4_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 5: #if the first order of features is 5, search by feature 5
                                if range_u_min_1 <= L_feature_5_float[j] <= range_w_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        

        #if the 1st node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_1 < range_u_max_1: 
                paths[ii] = index_feature_1
                for j in range(len(L_classification)):
                        if index_feature_1 == 1: #if the first order of features is 1, search by feature 1
                                if range_w_min_1 <= L_feature_1_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 2: #if the first order of features is 2, search by feature 2
                                if range_w_min_1 <= L_feature_2_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 3: #if the first order of features is 3, search by feature 3
                                if range_w_min_1 <= L_feature_3_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
                        if index_feature_1 == 4: #if the first order of features is 4, search by feature 4
                                if range_w_min_1 <= L_feature_4_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                
                        if index_feature_1 == 5: #if the first order of features is 5, search by feature 5
                                if range_w_min_1 <= L_feature_5_float[j] <= range_u_max_1:
                                        L_classification_a1.append(L_classification[j])
                                        L_feature_1_a1.append(L_feature_1_float[j])
                                        L_feature_2_a1.append(L_feature_2_float[j])
                                        L_feature_3_a1.append(L_feature_3_float[j])
                                        L_feature_4_a1.append(L_feature_4_float[j])
                                        L_feature_5_a1.append(L_feature_5_float[j])
                                        
        else: #if error occurs, report error
                print(1)
                print(range_w_max_1)
                print(range_w_min_1)
                print(range_u_max_1)
                print(range_u_min_1)
                
        #if the 2st node deals with the 1st feature, compute the ranges of this feature's values for different classes
        if index_feature_2 == 1:
                range_w_min_2 = L_feature_1_a1[0]
                range_w_max_2 = L_feature_1_a1[0]
                range_u_min_2 = L_feature_1_a1[0]
                range_u_max_2 = L_feature_1_a1[0]
                for i in range(len(L_classification_a1)):
                        if L_classification_a1[i] == 'w':
                                if L_feature_1_a1[i] < range_w_min_2:
                                        range_w_min_2 = L_feature_1_a1[i]
                                if L_feature_1_a1[i] > range_w_max_2:
                                        range_w_max_2 = L_feature_1_a1[i]
                        if L_classification_a1[i] == 'u':
                                if L_feature_1_a1[i] < range_u_min_2:
                                        range_u_min_2 = L_feature_1_a1[i]
                                if L_feature_1_a1[i] > range_u_max_2:
                                        range_u_max_2 = L_feature_1_a1[i]

        #if the 2nd node deals with the 2nd feature, compute the ranges of this feature's values for different classes
        if index_feature_2 == 2:
                range_w_min_2 = L_feature_2_a1[0]
                range_w_max_2 = L_feature_2_a1[0]
                range_u_min_2 = L_feature_2_a1[0]
                range_u_max_2 = L_feature_2_a1[0]
                for i in range(len(L_classification_a1)):
                        if L_classification_a1[i] == 'w':
                                if L_feature_2_a1[i] < range_w_min_2:
                                        range_w_min_2 = L_feature_2_a1[i]
                                if L_feature_2_a1[i] > range_w_max_2:
                                        range_w_max_2 = L_feature_2_a1[i]
                        if L_classification_a1[i] == 'u':
                                if L_feature_2_a1[i] < range_u_min_2:
                                        range_u_min_2 = L_feature_2_a1[i]
                                if L_feature_2_a1[i] > range_u_max_2:
                                        range_u_max_2 = L_feature_2_a1[i] 

        #if the 2nd node deals with the 3rd feature, compute the ranges of this feature's values for different classes    
        if index_feature_2 == 3:
                range_w_min_2 = L_feature_3_a1[0]
                range_w_max_2 = L_feature_3_a1[0]
                range_u_min_2 = L_feature_3_a1[0]
                range_u_max_2 = L_feature_3_a1[0]
                for i in range(len(L_classification_a1)):
                        if L_classification_a1[i] == 'w':
                                if L_feature_3_a1[i] < range_w_min_2:
                                        range_w_min_2 = L_feature_3_a1[i]
                                if L_feature_3_a1[i] > range_w_max_2:
                                        range_w_max_2 = L_feature_3_a1[i]
                        if L_classification_a1[i] == 'u':
                                if L_feature_3_a1[i] < range_u_min_2:
                                        range_u_min_2 = L_feature_3_a1[i]
                                if L_feature_3_a1[i] > range_u_max_2:
                                        range_u_max_2 = L_feature_3_a1[i]
                
        #if the 2nd node deals with the 4th feature, compute the ranges of this feature's values for different classes
        if index_feature_2 == 4:
                range_w_min_2 = L_feature_4_a1[0]
                range_w_max_2 = L_feature_4_a1[0]
                range_u_min_2 = L_feature_4_a1[0]
                range_u_max_2 = L_feature_4_a1[0]
                for i in range(len(L_classification_a1)):
                        if L_classification_a1[i] == 'w':
                                if L_feature_4_a1[i] < range_w_min_2:
                                        range_w_min_2 = L_feature_4_a1[i]
                                if L_feature_4_a1[i] > range_w_max_2:
                                        range_w_max_2 = L_feature_4_a1[i]
                        if L_classification_a1[i] == 'u':
                                if L_feature_4_a1[i] < range_u_min_2:
                                        range_u_min_2 = L_feature_4_a1[i]
                                if L_feature_4_a1[i] > range_u_max_2:
                                        range_u_max_2 = L_feature_4_a1[i]
                
        #if the 2nd node deals with the 5th feature, compute the ranges of this feature's values for different classes
        if index_feature_2 == 5:
                range_w_min_2 = L_feature_5_a1[0]
                range_w_max_2 = L_feature_5_a1[0]
                range_u_min_2 = L_feature_5_a1[0]
                range_u_max_2 = L_feature_5_a1[0]
                for i in range(len(L_classification_a1)):
                        if L_classification_a1[i] == 'w':
                                if L_feature_5_a1[i] < range_w_min_2:
                                        range_w_min_2 = L_feature_5_a1[i]
                                if L_feature_5_a1[i] > range_w_max_2:
                                        range_w_max_2 = L_feature_5_a1[i]
                        if L_classification_a1[i] == 'u':
                                if L_feature_5_a1[i] < range_u_min_2:
                                        range_u_min_2 = L_feature_5_a1[i]
                                if L_feature_5_a1[i] > range_u_max_2:
                                        range_u_max_2 = L_feature_5_a1[i]

        #if the 2nd node can identify all training examples, work done. Disable the following procedures.
        if range_w_max_2 < range_u_min_2:
                index_feature_3 = 0
                index_feature_4 = 0
                index_feature_5 = 0
                paths[ii] = paths[ii]*10 + index_feature_2
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 2nd node can identify all training examples, work done. Disable the following procedures.
        elif range_u_max_2 < range_w_min_2:
                index_feature_3 = 0
                index_feature_4 = 0
                index_feature_5 = 0
                paths[ii] = paths[ii]*10 + index_feature_2
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 2nd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_2 <= range_u_min_2 and range_u_min_2 < range_u_max_2 and range_u_max_2 <= range_w_max_2:
                paths[ii] = paths[ii]*10 + index_feature_2
                for j in range(len(L_classification_a1)):
                        if index_feature_2 == 1: #if the second order of features is 1, search by feature 1
                                if range_u_min_2 <= L_feature_1_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 2: #if the second order of features is 2, search by feature 2
                                if range_u_min_2 <= L_feature_2_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 3: #if the second order of features is 3, search by feature 3
                                if range_u_min_2 <= L_feature_3_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 4: #if the second order of features is 4, search by feature 4
                                if range_u_min_2 <= L_feature_4_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 5: #if the second order of features is 5, search by feature 5
                                if range_u_min_2 <= L_feature_5_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                                
        #if the 2nd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_2 <= range_w_min_2 and range_w_min_2 < range_w_max_2 and range_w_max_2 <= range_u_max_2: #intersection is w
                paths[ii] = paths[ii]*10 + index_feature_2
                for j in range(len(L_classification_a1)):
                        if index_feature_2 == 1: #if the second order of features is 1, search by feature 1
                                if range_w_min_2 <= L_feature_1_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 2: #if the second order of features is 2, search by feature 2
                                if range_w_min_2 <= L_feature_2_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 3: #if the second order of features is 3, search by feature 3
                                if range_w_min_2 <= L_feature_3_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 4: #if the second order of features is 4, search by feature 4
                                if range_w_min_2 <= L_feature_4_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 5: #if the second order of features is 5, search by feature 5
                                if range_w_min_2 <= L_feature_5_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])

        #if the 2nd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_2 < range_w_max_2: 
                paths[ii] = paths[ii]*10 + index_feature_2
                for j in range(len(L_classification_a1)):
                        if index_feature_2 == 1: #if the second order of features is 1, search by feature 1
                                if range_u_min_2 <= L_feature_1_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 2: #if the second order of features is 2, search by feature 2
                                if range_u_min_2 <= L_feature_2_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 3: #if the second order of features is 3, search by feature 3
                                if range_u_min_2 <= L_feature_3_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 4: #if the second order of features is 4, search by feature 4
                                if range_u_min_2 <= L_feature_4_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 5: #if the second order of features is 5, search by feature 5
                                if range_u_min_2 <= L_feature_5_a1[j] <= range_w_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])

        #if the 2nd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_2 < range_u_max_2: 
                paths[ii] = paths[ii]*10 + index_feature_2
                for j in range(len(L_classification_a1)):
                        if index_feature_2 == 1: #if the second order of features is 1, search by feature 1
                                if range_w_min_2 <= L_feature_1_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 2: #if the second order of features is 2, search by feature 2
                                if range_w_min_2 <= L_feature_2_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 3: #if the second order of features is 3, search by feature 3
                                if range_w_min_2 <= L_feature_3_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 4: #if the second order of features is 4, search by feature 4
                                if range_w_min_2 <= L_feature_4_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
                        if index_feature_2 == 5: #if the second order of features is 5, search by feature 5
                                if range_w_min_2 <= L_feature_5_a1[j] <= range_u_max_2:
                                        L_classification_a2.append(L_classification_a1[j])
                                        L_feature_1_a2.append(L_feature_1_a1[j])
                                        L_feature_2_a2.append(L_feature_2_a1[j])
                                        L_feature_3_a2.append(L_feature_3_a1[j])
                                        L_feature_4_a2.append(L_feature_4_a1[j])
                                        L_feature_5_a2.append(L_feature_5_a1[j])
        else:
                #if error occurs, report error
                print(2)
                print(range_w_max_2)
                print(range_w_min_2)
                print(range_u_max_2)
                print(range_u_min_2)
                
        #if the 3rd node deals with the 1st feature, compute the ranges of this feature's values for different classes
        if index_feature_3 == 1:
                range_w_min_3 = L_feature_1_a2[0]
                range_w_max_3 = L_feature_1_a2[0]
                range_u_min_3 = L_feature_1_a2[0]
                range_u_max_3 = L_feature_1_a2[0]
                for i in range(len(L_classification_a2)):
                        if L_classification_a2[i] == 'w':
                                if L_feature_1_a2[i] < range_w_min_3:
                                        range_w_min_3 = L_feature_1_a2[i]
                                if L_feature_1_a2[i] > range_w_max_3:
                                        range_w_max_3 = L_feature_1_a2[i]
                        if L_classification_a2[i] == 'u':
                                if L_feature_1_a2[i] < range_u_min_3:
                                        range_u_min_3 = L_feature_1_a2[i]
                                if L_feature_1_a2[i] > range_u_max_3:
                                        range_u_max_3 = L_feature_1_a2[i]

        #if the 3rd node deals with the 2nd feature, compute the ranges of this feature's values for different classes       
        if index_feature_3 == 2:
                range_w_min_3 = L_feature_2_a2[0]
                range_w_max_3 = L_feature_2_a2[0]
                range_u_min_3 = L_feature_2_a2[0]
                range_u_max_3 = L_feature_2_a2[0]
                for i in range(len(L_classification_a2)):
                        if L_classification_a2[i] == 'w':
                                if L_feature_2_a2[i] < range_w_min_3:
                                        range_w_min_3 = L_feature_2_a2[i]
                                if L_feature_2_a2[i] > range_w_max_3:
                                        range_w_max_3 = L_feature_2_a2[i]
                        if L_classification_a2[i] == 'u':
                                if L_feature_2_a2[i] < range_u_min_3:
                                        range_u_min_3 = L_feature_2_a2[i]
                                if L_feature_2_a2[i] > range_u_max_3:
                                        range_u_max_3 = L_feature_2_a2[i]
                
        #if the 3rd node deals with the 3rd feature, compute the ranges of this feature's values for different classes
        if index_feature_3 == 3:
                range_w_min_3 = L_feature_3_a2[0]
                range_w_max_3 = L_feature_3_a2[0]
                range_u_min_3 = L_feature_3_a2[0]
                range_u_max_3 = L_feature_3_a2[0]
                for i in range(len(L_classification_a2)):
                        if L_classification_a2[i] == 'w':
                                if L_feature_3_a2[i] < range_w_min_3:
                                        range_w_min_3 = L_feature_3_a2[i]
                                if L_feature_3_a2[i] > range_w_max_3:
                                        range_w_max_3 = L_feature_3_a2[i]
                        if L_classification_a2[i] == 'u':
                                if L_feature_3_a2[i] < range_u_min_3:
                                        range_u_min_3 = L_feature_3_a2[i]
                                if L_feature_3_a2[i] > range_u_max_3:
                                        range_u_max_3 = L_feature_3_a2[i]
                
        #if the 3rd node deals with the 4th feature, compute the ranges of this feature's values for different classes
        if index_feature_3 == 4:
                range_w_min_3 = L_feature_4_a2[0]
                range_w_max_3 = L_feature_4_a2[0]
                range_u_min_3 = L_feature_4_a2[0]
                range_u_max_3 = L_feature_4_a2[0]
                for i in range(len(L_classification_a2)):
                        if L_classification_a2[i] == 'w':
                                if L_feature_4_a2[i] < range_w_min_3:
                                        range_w_min_3 = L_feature_4_a2[i]
                                if L_feature_4_a2[i] > range_w_max_3:
                                        range_w_max_3 = L_feature_4_a2[i]
                        if L_classification_a2[i] == 'u':
                                if L_feature_4_a2[i] < range_u_min_3:
                                        range_u_min_3 = L_feature_4_a2[i]
                                if L_feature_4_a2[i] > range_u_max_3:
                                        range_u_max_3 = L_feature_4_a2[i]
                
        #if the 3rd node deals with the 5th feature, compute the ranges of this feature's values for different classes
        if index_feature_3 == 5:
                range_w_min_3 = L_feature_5_a2[0]
                range_w_max_3 = L_feature_5_a2[0]
                range_u_min_3 = L_feature_5_a2[0]
                range_u_max_3 = L_feature_5_a2[0]
                for i in range(len(L_classification_a2)):
                        if L_classification_a2[i] == 'w':
                                if L_feature_5_a2[i] < range_w_min_3:
                                        range_w_min_3 = L_feature_5_a2[i]
                                if L_feature_5_a2[i] > range_w_max_3:
                                        range_w_max_3 = L_feature_5_a2[i]
                        if L_classification_a2[i] == 'u':
                                if L_feature_5_a2[i] < range_u_min_3:
                                        range_u_min_3 = L_feature_5_a2[i]
                                if L_feature_5_a2[i] > range_u_max_3:
                                        range_u_max_3 = L_feature_5_a2[i]

        #if the 3rd node can identify all training examples, work done. Disable the following procedures.
        if range_w_max_3 < range_u_min_3:
                index_feature_4 = 0
                index_feature_5 = 0
                paths[ii] = paths[ii]*10 + index_feature_3
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 3rd node can identify all training examples, work done. Disable the following procedures.
        elif range_u_max_3 < range_w_min_3:
                index_feature_4 = 0
                index_feature_5 = 0
                paths[ii] = paths[ii]*10 + index_feature_3
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 3rd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_3 <= range_u_min_3 and range_u_min_3 < range_u_max_3 and range_u_max_3 <= range_w_max_3: #intersection is u
                paths[ii] = paths[ii]*10 + index_feature_3
                for j in range(len(L_classification_a2)):
                        if index_feature_3 == 1: #if the 3rd order of features is 1, search by feature 1
                                if range_u_min_3 <= L_feature_1_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 2: #if the 3rd order of features is 2, search by feature 2
                                if range_u_min_3 <= L_feature_2_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 3: #if the 3rd order of features is 3, search by feature 3
                                if range_u_min_3 <= L_feature_3_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 4: #if the 3rd order of features is 4, search by feature 4
                                if range_u_min_3 <= L_feature_4_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 5: #if the 3rd order of features is 5, search by feature 5
                                if range_u_min_3 <= L_feature_5_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                                

        #if the 3rd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_3 <= range_w_min_3 and range_w_min_3 < range_w_max_3 and range_w_max_3 <= range_u_max_3: #intersection is w
                paths[ii] = paths[ii]*10 + index_feature_3
                for j in range(len(L_classification_a2)):
                        if index_feature_3 == 1: #if the 3rd order of features is 1, search by feature 1
                                if range_w_min_3 <= L_feature_1_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 2: #if the 3rd order of features is 2, search by feature 2
                                if range_w_min_3 <= L_feature_2_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 3: #if the 3rd order of features is 3, search by feature 3
                                if range_w_min_3 <= L_feature_3_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 4: #if the 3rd order of features is 4, search by feature 4
                                if range_w_min_3 <= L_feature_4_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 5: #if the 3rd order of features is 5, search by feature 5
                                if range_w_min_3 <= L_feature_5_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])

        #if the 3rd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_3 < range_w_max_3: #intersection
                paths[ii] = paths[ii]*10 + index_feature_3
                for j in range(len(L_classification_a2)):
                        if index_feature_3 == 1: #if the 3rd order of features is 1, search by feature 1
                                if range_u_min_3 <= L_feature_1_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 2: #if the 3rd order of features is 2, search by feature 2
                                if range_u_min_3 <= L_feature_2_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 3: #if the 3rd order of features is 3, search by feature 3
                                if range_u_min_3 <= L_feature_3_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 4: #if the 3rd order of features is 4, search by feature 4
                                if range_u_min_3 <= L_feature_4_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 5: #if the 3rd order of features is 5, search by feature 5
                                if range_u_min_3 <= L_feature_5_a2[j] <= range_w_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])

        #if the 3rd node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_3 < range_u_max_3: #intersection
                paths[ii] = paths[ii]*10 + index_feature_3
                for j in range(len(L_classification_a2)):
                        if index_feature_3 == 1: #if the 3rd order of features is 1, search by feature 1
                                if range_w_min_3 <= L_feature_1_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 2: #if the 3rd order of features is 2, search by feature 2
                                if range_w_min_3 <= L_feature_2_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 3: #if the 3rd order of features is 3, search by feature 3
                                if range_w_min_3 <= L_feature_3_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 4: #if the 3rd order of features is 4, search by feature 4
                                if range_w_min_3 <= L_feature_4_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
                        if index_feature_3 == 5: #if the 3rd order of features is 5, search by feature 5
                                if range_w_min_3 <= L_feature_5_a2[j] <= range_u_max_3:
                                        L_classification_a3.append(L_classification_a2[j])
                                        L_feature_1_a3.append(L_feature_1_a2[j])
                                        L_feature_2_a3.append(L_feature_2_a2[j])
                                        L_feature_3_a3.append(L_feature_3_a2[j])
                                        L_feature_4_a3.append(L_feature_4_a2[j])
                                        L_feature_5_a3.append(L_feature_5_a2[j])
        else:
                #if error occurs, report error
                print(3)
                print(range_w_max_3)
                print(range_w_min_3)
                print(range_u_max_3)
                print(range_u_min_3)
                
        #if the 4th node deals with the 1st feature, compute the ranges of this feature's values for different classes
        if index_feature_4 == 1:
                range_w_min_4 = L_feature_1_a3[0]
                range_w_max_4 = L_feature_1_a3[0]
                range_u_min_4 = L_feature_1_a3[0]
                range_u_max_4 = L_feature_1_a3[0]
                for i in range(len(L_classification_a3)):
                        if L_classification_a3[i] == 'w':
                                if L_feature_1_a3[i] < range_w_min_4:
                                        range_w_min_4 = L_feature_1_a3[i]
                                if L_feature_1_a3[i] > range_w_max_4:
                                        range_w_max_4 = L_feature_1_a3[i]
                        if L_classification_a3[i] == 'u':
                                if L_feature_1_a3[i] < range_u_min_4:
                                        range_u_min_4 = L_feature_1_a3[i]
                                if L_feature_1_a3[i] > range_u_max_4:
                                        range_u_max_4 = L_feature_1_a3[i]
                
        #if the 4th node deals with the 2nd feature, compute the ranges of this feature's values for different classes
        if index_feature_4 == 2:
                range_w_min_4 = L_feature_2_a3[0]
                range_w_max_4 = L_feature_2_a3[0]
                range_u_min_4 = L_feature_2_a3[0]
                range_u_max_4 = L_feature_2_a3[0]
                for i in range(len(L_classification_a3)):
                        if L_classification_a3[i] == 'w':
                                if L_feature_2_a3[i] < range_w_min_4:
                                        range_w_min_4 = L_feature_2_a3[i]
                                if L_feature_2_a3[i] > range_w_max_4:
                                        range_w_max_4 = L_feature_2_a3[i]
                        if L_classification_a3[i] == 'u':
                                if L_feature_2_a3[i] < range_u_min_4:
                                        range_u_min_4 = L_feature_2_a3[i]
                                if L_feature_2_a3[i] > range_u_max_4:
                                        range_u_max_4 = L_feature_2_a3[i]
                
        #if the 4th node deals with the 3rd feature, compute the ranges of this feature's values for different classes
        if index_feature_4 == 3:
                range_w_min_4 = L_feature_3_a3[0]
                range_w_max_4 = L_feature_3_a3[0]
                range_u_min_4 = L_feature_3_a3[0]
                range_u_max_4 = L_feature_3_a3[0]
                for i in range(len(L_classification_a3)):
                        if L_classification_a3[i] == 'w':
                                if L_feature_3_a3[i] < range_w_min_4:
                                        range_w_min_4 = L_feature_3_a3[i]
                                if L_feature_3_a3[i] > range_w_max_4:
                                        range_w_max_4 = L_feature_3_a3[i]
                        if L_classification_a3[i] == 'u':
                                if L_feature_3_a3[i] < range_u_min_4:
                                        range_u_min_4 = L_feature_3_a3[i]
                                if L_feature_3_a3[i] > range_u_max_4:
                                        range_u_max_4 = L_feature_3_a3[i]
                
        #if the 4th node deals with the 4th feature, compute the ranges of this feature's values for different classes
        if index_feature_4 == 4:
                range_w_min_4 = L_feature_4_a3[0]
                range_w_max_4 = L_feature_4_a3[0]
                range_u_min_4 = L_feature_4_a3[0]
                range_u_max_4 = L_feature_4_a3[0]
                for i in range(len(L_classification_a3)):
                        if L_classification_a3[i] == 'w':
                                if L_feature_4_a3[i] < range_w_min_4:
                                        range_w_min_4 = L_feature_4_a3[i]
                                if L_feature_4_a3[i] > range_w_max_4:
                                        range_w_max_4 = L_feature_4_a3[i]
                        if L_classification_a3[i] == 'u':
                                if L_feature_4_a3[i] < range_u_min_4:
                                        range_u_min_4 = L_feature_4_a3[i]
                                if L_feature_4_a3[i] > range_u_max_4:
                                        range_u_max_4 = L_feature_4_a3[i]
                
        #if the 4th node deals with the 5th feature, compute the ranges of this feature's values for different classes
        if index_feature_4 == 5:
                range_w_min_4 = L_feature_5_a3[0]
                range_w_max_4 = L_feature_5_a3[0]
                range_u_min_4 = L_feature_5_a3[0]
                range_u_max_4 = L_feature_5_a3[0]
                for i in range(len(L_classification_a3)):
                        if L_classification_a3[i] == 'w':
                                if L_feature_5_a3[i] < range_w_min_4:
                                        range_w_min_4 = L_feature_5_a3[i]
                                if L_feature_5_a3[i] > range_w_max_4:
                                        range_w_max_4 = L_feature_5_a3[i]
                        if L_classification_a3[i] == 'u':
                                if L_feature_5_a3[i] < range_u_min_4:
                                        range_u_min_4 = L_feature_5_a3[i]
                                if L_feature_5_a3[i] > range_u_max_4:
                                        range_u_max_4 = L_feature_5_a3[i]

        #if the 4th node can identify all training examples, work done. Disable the following procedures.
        if range_w_max_4 < range_u_min_4: 
                index_feature_5 = 0
                paths[ii] = paths[ii]*10 + index_feature_4
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 4th node can identify all training examples, work done. Disable the following procedures.
        elif range_u_max_4 < range_w_min_4:
                index_feature_5 = 0
                paths[ii] = paths[ii]*10 + index_feature_4
                results[ii] = 0
                print(paths[ii])
                print(results[ii])

        #if the 4th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_4 <= range_u_min_4 and range_u_min_4 < range_u_max_4 and range_u_max_4 <= range_w_max_4: #intersection is u
                paths[ii] = paths[ii]*10 + index_feature_4
                for j in range(len(L_classification_a3)):
                        if index_feature_4 == 1: #if the 4th order of features is 1, search by feature 1
                                if range_u_min_4 <= L_feature_1_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 2: #if the 4th order of features is 2, search by feature 2
                                if range_u_min_4 <= L_feature_2_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 3: #if the 4th order of features is 3, search by feature 3
                                if range_u_min_4 <= L_feature_3_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 4: #if the 4th order of features is 4, search by feature 4
                                if range_u_min_4 <= L_feature_4_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 5: #if the 4th order of features is 5, search by feature 5
                                if range_u_min_4 <= L_feature_5_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                                
        #if the 4th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_4 <= range_w_min_4 and range_w_min_4 < range_w_max_4 and range_w_max_4 <= range_u_max_4: #intersection is w
                paths[ii] = paths[ii]*10 + index_feature_4
                for j in range(len(L_classification_a3)):
                        if index_feature_4 == 1: #if the 4th order of features is 1, search by feature 1
                                if range_w_min_4 <= L_feature_1_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 2: #if the 4th order of features is 2, search by feature 2
                                if range_w_min_4 <= L_feature_2_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 3: #if the 4th order of features is 3, search by feature 3
                                if range_w_min_4 <= L_feature_3_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 4: #if the 4th order of features is 4, search by feature 4
                                if range_w_min_4 <= L_feature_4_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 5: #if the 4th order of features is 5, search by feature 5
                                if range_w_min_4 <= L_feature_5_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])

        #if the 4th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_4 < range_w_max_4: #intersection
                paths[ii] = paths[ii]*10 + index_feature_4
                for j in range(len(L_classification_a3)):
                        if index_feature_4 == 1: #if the 4th order of features is 1, search by feature 1
                                if range_u_min_4 <= L_feature_1_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 2: #if the 4th order of features is 2, search by feature 2
                                if range_u_min_4 <= L_feature_2_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 3: #if the 4th order of features is 3, search by feature 3
                                if range_u_min_4 <= L_feature_3_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 4: #if the 4th order of features is 4, search by feature 4
                                if range_u_min_4 <= L_feature_4_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 5: #if the 4th order of features is 5, search by feature 5
                                if range_u_min_4 <= L_feature_5_a3[j] <= range_w_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])

        #if the 4th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_4 < range_u_max_4: #intersection
                paths[ii] = paths[ii]*10 + index_feature_4
                for j in range(len(L_classification_a3)):
                        if index_feature_4 == 1: #if the 4th order of features is 1, search by feature 1
                                if range_w_min_4 <= L_feature_1_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 2: #if the 4th order of features is 2, search by feature 2
                                if range_w_min_4 <= L_feature_2_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 3: #if the 4th order of features is 3, search by feature 3
                                if range_w_min_4 <= L_feature_3_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 4: #if the 4th order of features is 4, search by feature 4
                                if range_w_min_4 <= L_feature_4_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
                        if index_feature_4 == 5: #if the 4th order of features is 5, search by feature 5
                                if range_w_min_4 <= L_feature_5_a3[j] <= range_u_max_4:
                                        L_classification_a4.append(L_classification_a3[j])
                                        L_feature_1_a4.append(L_feature_1_a3[j])
                                        L_feature_2_a4.append(L_feature_2_a3[j])
                                        L_feature_3_a4.append(L_feature_3_a3[j])
                                        L_feature_4_a4.append(L_feature_4_a3[j])
                                        L_feature_5_a4.append(L_feature_5_a3[j])
        else:
                #if error occurs, report error
                print(4)
                print(range_w_max_4)
                print(range_w_min_4)
                print(range_u_max_4)
                print(range_u_min_4)
                        
        #if the 5th node deals with the 1st feature, compute the ranges of this feature's values for different classes
        if index_feature_5 == 1:
                range_w_min_5 = L_feature_1_a4[0]
                range_w_max_5 = L_feature_1_a4[0]
                range_u_min_5 = L_feature_1_a4[0]
                range_u_max_5 = L_feature_1_a4[0]
                for i in range(len(L_classification_a4)):
                        if L_classification_a4[i] == 'w':
                                if L_feature_1_a4[i] < range_w_min_5:
                                        range_w_min_5 = L_feature_1_a4[i]
                                if L_feature_1_a4[i] > range_w_max_5:
                                        range_w_max_5 = L_feature_1_a4[i]
                        if L_classification_a4[i] == 'u':
                                if L_feature_1_a4[i] < range_u_min_5:
                                        range_u_min_5 = L_feature_1_a4[i]
                                if L_feature_1_a4[i] > range_u_max_5:
                                        range_u_max_5 = L_feature_1_a4[i]
                
        #if the 5th node deals with the 2nd feature, compute the ranges of this feature's values for different classes
        if index_feature_5 == 2:
                range_w_min_5 = L_feature_2_a4[0]
                range_w_max_5 = L_feature_2_a4[0]
                range_u_min_5 = L_feature_2_a4[0]
                range_u_max_5 = L_feature_2_a4[0]
                for i in range(len(L_classification_a4)):
                        if L_classification_a4[i] == 'w':
                                if L_feature_2_a4[i] < range_w_min_5:
                                        range_w_min_5 = L_feature_2_a4[i]
                                if L_feature_2_a4[i] > range_w_max_5:
                                        range_w_max_5 = L_feature_2_a4[i]
                        if L_classification_a4[i] == 'u':
                                if L_feature_2_a4[i] < range_u_min_5:
                                        range_u_min_5 = L_feature_2_a4[i]
                                if L_feature_2_a4[i] > range_u_max_5:
                                        range_u_max_5 = L_feature_2_a4[i]
                
        #if the 5th node deals with the 3rd feature, compute the ranges of this feature's values for different classes
        if index_feature_5 == 3:
                range_w_min_5 = L_feature_3_a4[0]
                range_w_max_5 = L_feature_3_a4[0]
                range_u_min_5 = L_feature_3_a4[0]
                range_u_max_5 = L_feature_3_a4[0]
                for i in range(len(L_classification_a4)):
                        if L_classification_a4[i] == 'w':
                                if L_feature_3_a4[i] < range_w_min_5:
                                        range_w_min_5 = L_feature_3_a4[i]
                                if L_feature_3_a4[i] > range_w_max_5:
                                        range_w_max_5 = L_feature_3_a4[i]
                        if L_classification_a4[i] == 'u':
                                if L_feature_3_a4[i] < range_u_min_5:
                                        range_u_min_5 = L_feature_3_a4[i]
                                if L_feature_3_a4[i] > range_u_max_5:
                                        range_u_max_5 = L_feature_3_a4[i]
                
        #if the 5th node deals with the 4th feature, compute the ranges of this feature's values for different classes
        if index_feature_5 == 4:
                range_w_min_5 = L_feature_4_a4[0]
                range_w_max_5 = L_feature_4_a4[0]
                range_u_min_5 = L_feature_4_a4[0]
                range_u_max_5 = L_feature_4_a4[0]
                for i in range(len(L_classification_a4)):
                        if L_classification_a4[i] == 'w':
                                if L_feature_4_a4[i] < range_w_min_5:
                                        range_w_min_5 = L_feature_4_a4[i]
                                if L_feature_4_a4[i] > range_w_max_5:
                                        range_w_max_5 = L_feature_4_a4[i]
                        if L_classification_a4[i] == 'u':
                                if L_feature_4_a4[i] < range_u_min_5:
                                        range_u_min_5 = L_feature_4_a4[i]
                                if L_feature_4_a4[i] > range_u_max_5:
                                        range_u_max_5 = L_feature_4_a4[i]
                
        #if the 5th node deals with the 5th feature, compute the ranges of this feature's values for different classes
        if index_feature_5 == 5:
                range_w_min_5 = L_feature_5_a4[0]
                range_w_max_5 = L_feature_5_a4[0]
                range_u_min_5 = L_feature_5_a4[0]
                range_u_max_5 = L_feature_5_a4[0]
                for i in range(len(L_classification_a4)):
                        if L_classification_a4[i] == 'w':
                                if L_feature_5_a4[i] < range_w_min_5:
                                        range_w_min_5 = L_feature_5_a4[i]
                                if L_feature_5_a4[i] > range_w_max_5:
                                        range_w_max_5 = L_feature_5_a4[i]
                        if L_classification_a4[i] == 'u':
                                if L_feature_5_a4[i] < range_u_min_5:
                                        range_u_min_5 = L_feature_5_a4[i]
                                if L_feature_5_a4[i] > range_u_max_5:
                                        range_u_max_5 = L_feature_5_a4[i]

        #if the 5th node can identify all training examples, work done.
        if range_w_max_5 < range_u_min_5: 
                paths[ii] = paths[ii]*10 + index_feature_5
                results[ii] = 0
                print('Candidate Decision Tree Structure:')
                print(paths[ii])
                print('total number of training examples which cannot be identified after training:')
                print(results[ii])

        #if the 5th node can identify all training examples, work done.
        elif range_u_max_5 < range_w_min_5: 
                paths[ii] = paths[ii]*10 + index_feature_5
                results[ii] = 0
                print('Candidate Decision Tree Structure:')
                print(paths[ii])
                print('total number of training examples which cannot be identified after training:')
                print(results[ii])

        #if the 5th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_5 <= range_u_min_5 and range_u_min_5 < range_u_max_5 and range_u_max_5 <= range_w_max_5: #intersection is u
                paths[ii] = paths[ii]*10 + index_feature_5
                for j in range(len(L_classification_a4)):
                        if index_feature_5 == 1: #if the 5th order of features is 1, search by feature 1
                                if range_u_min_5 <= L_feature_1_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 2: #if the 5th order of features is 2, search by feature 2
                                if range_u_min_5 <= L_feature_2_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 3: #if the 5th order of features is 3, search by feature 3
                                if range_u_min_5 <= L_feature_3_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 4: #if the 5th order of features is 4, search by feature 4
                                if range_u_min_5 <= L_feature_4_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 5: #if the 5th order of features is 5, search by feature 5
                                if range_u_min_5 <= L_feature_5_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                results[ii] = len(L_classification_a5)
                print('Candidate Decision Tree Structure:')
                print(paths[ii])
                print('total number of training examples which cannot be identified after training:')
                print(results[ii])
                                
        #if the 5th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_5 <= range_w_min_5 and range_w_min_5 < range_w_max_5 and range_w_max_5 <= range_u_max_5: #intersection is w
                paths[ii] = paths[ii]*10 + index_feature_5
                for j in range(len(L_classification_a4)):
                        if index_feature_5 == 1: #if the 5th order of features is 1, search by feature 1
                                if range_w_min_5 <= L_feature_1_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 2: #if the 5th order of features is 2, search by feature 2
                                if range_w_min_5 <= L_feature_2_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 3: #if the 5th order of features is 3, search by feature 3
                                if range_w_min_5 <= L_feature_3_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 4: #if the 5th order of features is 4, search by feature 4
                                if range_w_min_5 <= L_feature_4_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 5: #if the 5th order of features is 5, search by feature 5
                                if range_w_min_5 <= L_feature_5_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                results[ii] = len(L_classification_a5)
                print('Candidate Decision Tree Structure:')
                print(paths[ii])
                print('total number of training examples which cannot be identified after training:')
                print(results[ii])

        #if the 5th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_u_min_5 < range_w_max_5: #intersection
                paths[ii] = paths[ii]*10 + index_feature_5
                for j in range(len(L_classification_a4)):
                        if index_feature_5 == 1: #if the 5th order of features is 1, search by feature 1
                                if range_u_min_5 <= L_feature_1_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 2: #if the 5th order of features is 2, search by feature 2
                                if range_u_min_5 <= L_feature_2_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 3: #if the 5th order of features is 3, search by feature 3
                                if range_u_min_5 <= L_feature_3_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 4: #if the 5th order of features is 4, search by feature 4
                                if range_u_min_5 <= L_feature_4_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 5: #if the 5th order of features is 5, search by feature 5
                                if range_u_min_5 <= L_feature_5_a4[j] <= range_w_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                results[ii] = len(L_classification_a5)
                print('Candidate Decision Tree Structure:')
                print(paths[ii])
                print('total number of training examples which cannot be identified after training:')
                print(results[ii])

        #if the 5th node cannot identify all training examples, search and store all training examples which cannot be identified.
        elif range_w_min_5 < range_u_max_5: #intersection
                paths[ii] = index_feature_5
                for j in range(len(L_classification_a4)):
                        if index_feature_5 == 1: #if the 5th order of features is 1, search by feature 1
                                if range_w_min_5 <= L_feature_1_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 2: #if the 5th order of features is 2, search by feature 2
                                if range_w_min_5 <= L_feature_2_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 3: #if the 5th order of features is 3, search by feature 3
                                if range_w_min_5 <= L_feature_3_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 4: #if the 5th order of features is 4, search by feature 4
                                if range_w_min_5 <= L_feature_4_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                        if index_feature_5 == 5: #if the 5th order of features is 5, search by feature 5
                                if range_w_min_5 <= L_feature_5_a4[j] <= range_u_max_5:
                                        L_classification_a5.append(L_classification_a4[j])
                                        L_feature_1_a5.append(L_feature_1_a4[j])
                                        L_feature_2_a5.append(L_feature_2_a4[j])
                                        L_feature_3_a5.append(L_feature_3_a4[j])
                                        L_feature_4_a5.append(L_feature_4_a4[j])
                                        L_feature_5_a5.append(L_feature_5_a4[j])
                results[ii] = len(L_classification_a5)
                print('Candidate Decision Tree Structure:')
                print(paths[ii])
                print('total number of training examples which cannot be identified after training:')
                print(results[ii])
        else:
                #if error occurs, report error
                print(5)
                print(range_w_max_5)
                print(range_w_min_5)
                print(range_u_max_5)
                print(range_u_min_5)

                
