import pandas as pd
import math
from sklearn.model_selection import KFold

# Function to determine the Prior probabilty.
def estimatePriorProbability(dataset):
    datasetLength = len(dataset)  #length of the dataset
    count0and1 = dataset['class'].value_counts().to_dict() #Ex: {0: 11, 1: 9}
    #print(count0and1)
    L1 = count0and1[1]/datasetLength #P(w) = no of 1's/ length of dataset
    L2 = count0and1[0]/datasetLength #P(n) = no of 0's/ length of dataset
    priorProbability = {(0,L2), (1,L1)}             #{(1, 0.45), (0, 0.55)}0075234445
    #print('priorProbability is: ',priorProbability) #{(0, 0.9829453791196128), (1, 0.017054620880387186)}
    return priorProbability

#funtion that determines mean and varience of the given dataset.
def estimateMeanAndSD(dataset):
    meanAndVarience = {} #initialize and list to store mean and varience for all the features in training dataset
    for label in [0,1]:
        currentSet = dataset[(dataset['class'])== label] #split each class to segregate features
        #print(currentSet)
        result = {}   #temporary list to store mean and varience of a feature
        for i in dataset.iloc[:,1:]:  #gives each feature column name
            result[i] = []
            mean = currentSet[i].mean()
            varience = math.pow(currentSet[i].std(), 2)
            #print('consolidated mean and varience is',mean, varience)#{label:{'header1':[mean, var], 'header2':[mean, var]....
            result[i].append(mean)
            result[i].append(varience)
        meanAndVarience[label]=result #appending the main list
        #print('mean', meanAndVarience)
    return meanAndVarience # {0: {'GLCM_pa0': [[126.0175108909091, 137.91180472720325]], 'Mea0_Gree0': [[224.902291

def estimatePostProbability(data, mean, varience):
    #formula to calculate maximum likelihood of x given y
    exponent = math.exp(-(math.pow(data-mean , 2) / (2 * varience)))
    return (1 / (math.sqrt(2 * math.pi * varience))) * exponent

#function to predict training samples based on prior probabilites,
# mean and varience values of training dataset
def bayesAlgorithm(dataset,priorProbability,meanAndVarience):
    predictions = {} #final prediction list
    dataset = pd.read_csv('testing.csv') #read the testing file.
    for row in dataset.iterrows():
        results={} #temp variable to store result.
        print('row', row[1][2])
        for label, value in priorProbability:
            p=0 #initialize posterior probability
            for i in dataset.iloc[:, 1:]:  #gives each feature column name
                data = row[1][i]
                print('row', row[1][i])
                mean = meanAndVarience[label][i][0] #obtain mean
                varience = meanAndVarience[label][i][1] #obtain varience
                #print('proir probability is :',value)
                #print('mean is ',mean, varience)
                #estimate posterior probability based on prior probabilites, mean and varience of training dataset
                postProbability = estimatePostProbability(data, mean, varience)
                #print('postProbability is: ', postProbability)
                if(postProbability > 0):
                    p +=  math.log(postProbability) #take log probability

            results[label] = p + math.log(value + 0.1) #added bias 0.1
        #print('results[label]: ', results)
        #prediction is the one that has maximum probability
        predictions[int(row[0])] = max([key for key in results.keys() if results[
            key] == results[max(results, key=results.get)]])
        #print('results.keys() is : ',results.keys())
    return predictions


#function to determine accuracy
def estimateAccuracy(data, results):
    sizeN = 313 #size of N samples in testing dataset
    sizeW = 187 #size of W samples in testing dataset
    success = 0
    successW =0
    size = len(data)
    i = 0
    for row in data.iterrows():
        #print('row is: ',row[1]["class"])
        #print('result is ',results[i])
        expected = row[1]["class"]
        outcome = results[i]
        if(expected == outcome):
            success += 1
            if(row[1]["class"]==1.0):
                successW +=1
        i += 1
    print('suces: ',success)
    successPercentage = (success / size) * 100
    successWPercentage = (successW/sizeW)*100 #provides sensitivity
    successNPercentage = ((success-successW) / sizeN) * 100 #provide specificity
    return successPercentage,successWPercentage,successNPercentage

#function to model training datset
def predictionAlgorithm(dataset):
        priorProbability = estimatePriorProbability(dataset)
        meanAndVarience = estimateMeanAndSD(dataset)
        prediction = bayesAlgorithm(dataset,priorProbability,meanAndVarience)
        print('predictions is:  ',prediction)
        return prediction

def main():
    trainfile = 'training.csv'
    testfile = 'testing.csv'
    trainDataset = pd.read_csv(trainfile)
    testDataset = pd.read_csv(testfile)
    #print('trainDataset is : ',trainDataset)
    #datasetLength = len(trainDataset)
    result = predictionAlgorithm(trainDataset)
    #print('result: ',result)
    accuracy = estimateAccuracy(testDataset, result)
    print('testing accuracy is: ',accuracy[0])
    print('wilt classified percentage(sensitivity): ',accuracy[1])
    print('Normal classified percentage(specificity): ', accuracy[2])
    print('Geometric mean is :', math.sqrt(accuracy[1]/100 * accuracy[0]/100))
    #accracy is:  359 ; wilt classified:  119
    #print('total ', (accuracy[0]+accuracy[1]))
    #print('totalaccuracy ', (accuracy[0] + accuracy[1])/4)


main()