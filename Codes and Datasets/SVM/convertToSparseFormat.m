% create a libsvm compatible file from a text file.
clear
clc
close all

% addpath to the libsvm toolbox
addpath('./libsvm-3.22/windows');
% addpath to the data
dirData = './libsvm-3.22'; 
addpath(dirData);

infile = 'testing_least1removed.csv' %enter the file name to be converted to sparse format.
outfile = 'testing_least1removed.test' %enter the equivalent sparse matrix outfile.

%file = 'wiltUpSampledtoauto.csv';
%file =load('training.csv');
file = infile;  
SPECTF = csvread(file); % read a csv file
labels = SPECTF(:, 1); % labels from the 1st column
features = SPECTF(:, 2:end); 
features_sparse = sparse(features); % features stored in sparse matrix
libsvmwrite(outfile, labels, features_sparse);
%save('qwerty.mat',labels, features_sparse);
fprintf('DONE!');