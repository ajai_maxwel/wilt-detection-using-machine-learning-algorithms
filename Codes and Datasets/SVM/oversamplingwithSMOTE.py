#Program used for oversampling minority dataset using SMOTE.
#Provide input file name, output file name and oversampling count.

import pandas as pd
import numpy as np
from imblearn.over_sampling import SMOTE

upsampleCount = 740  # set the total number of samples needed for minority dataset
infileName = 'training_least1removed.csv'  # set the input file name.
outfileName = 'training_least1removedU1500p.csv'
# Read the dataset
df = pd.read_csv(infileName)
# names=["class", "GLCM_pan","Mean_Green","Mean_Red","Mean_NIR","SD_pan"]
y = df['class']  # y contains the class value
# print(df['class'].value_counts())
X = df.drop('class', axis=1)  # x contains columns other than class-column
# print(y)
# print(X)
# ratio={1: 370, }
# ratio="auto  #provide 50:50 ratio

# Oversampling minority class with 5 nearest neighbours.
sm = SMOTE(ratio={1: upsampleCount, }, random_state=0, k=None, k_neighbors=5, m_neighbors=10, out_step=0.5, kind='svm')
X_resampled, y_resampled = sm.fit_sample(X, y)

df_X = pd.DataFrame(X_resampled)
df_Y = pd.DataFrame(y_resampled)

# print(X_resampled, y_resampled)
df_upsampled = pd.concat([df_Y, df_X], axis=1)  # concatenate X and Y to create new file
print(df_upsampled)
df_upsampled.to_csv(outfileName, sep=',', index=False)  # create a new comma separated file
# index = False prevents the index from saving to the output file.
# np.savetxt('output1.csv', final, delimiter=",")
print('--file successfully created--')