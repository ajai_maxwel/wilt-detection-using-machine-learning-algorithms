% svm classifier using libsvm.
% libsvm need to added to path for execution.
clear
clc
close all

%formula for scalling: value = y_lower + (y_upper-y_lower) * (value - y_min)/(y_max-y_min);

% addpath to the libsvm toolbox
addpath('./libsvm-3.22/matlab');
% addpath to the data
dirData = './libsvm-3.22'; 
addpath(dirData);
addpath('./libsvm-3.22/windows'); 

%enter training and testing dataset file name.
trainfileName = 'trainingMain.s.train';
testfileName = 'testingMain.s.test';
testWName ='testingW.s.test';
testNName = 'testingN.s.test';


%train_file = load('trainingMainU1000p.txt');
%train_label = train_file(:,1);
%train_data = train_file(:, 2:end);
[train_label,train_data ] = libsvmread(trainfileName);
%initialise best cv to 0
bestcv = 0;
%Create array for c and g.

cTestValues=[];
gTestValues=[];

%range of C as per base paper
for i = -2 : 31
    temp = 1.25^i;
    cTestValues = [cTestValues, temp];
end

%range of G as per base paper
for i = -30 : 25
    temp = 1.25^i;
    gTestValues = [gTestValues, temp];
end

%C and g were tested and the limit was reduced to [0.0001 100]

for c = [0.0001, 0.001, 0.004, 0.01, 0.04, 0.1, 0.4, 1, 4, 10, 100],
    for g = [0.0001, 0.001, 0.004, 0.01, 0.04, 0.1, 0.4, 1, 4, 10, 100],

%c values of 1.25^?2, 1.25^?1, . . .,1.25^31. 
%? values of 1.25^?30, 1.25^?29, . . ., 1.25^25.
%for c = cTestValues
%    for g = gTestValues
     %for n = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1],   
        cmd = ['-t 2 -v 5 -c ', num2str(c), ' -g ', num2str(g),];
        %cmd = ['-v 5 -c ', num2str(c),'-s 1 -n', num2str(n)];
        %since params are passed as stings in libsvm, num2str is used
        %5-fold cross validation is carried out.
        %kernal : RBF
        %calculates cross validation accuracy/mean squared error on them.
        %svmtrain is called from libsvm
        cv = svmtrain(train_label, train_data, cmd);      
        
        %when the current cv is higher than/equalto the bestcv so far.
        if (cv >= bestcv)
            bestcv = cv;
            bestc = c;
            bestg = g;
            
        end
        fprintf('%g %g %g (best c=%g, g=%g, rate=%g)\n', c, g, cv, bestc, bestg, bestcv);
     
    end
end

%Obtain best model with bestC and bestG
cmd = ['-t 2 -c ', num2str(bestc), ' -g ', num2str(bestg)];
model = svmtrain(train_label,train_data, cmd);


%--------------------------------------------------------------------------
%Step 3: Testing
%--------------------------------------------------------------------------
fprintf('\n----Loading and testing the data set----\n');
%loading the new testing dataset.
%test_file = load('dummyfile02W.test');
%test_label = test_file(:,1);
%test_data = test_file(:, 2:end);
[test_label,test_data ] = libsvmread(testfileName);
%p = svmpredict(test_label, test_data, model);
%using svmpredict to test 500 new samples and 
%knowing the accuracy of prediction.
[predict, accuracy, dec] = svmpredict(test_label, test_data, model);

fprintf('\n ----Loading and testing the N data set----\n');
%loading the new testing dataset.
[testN_label,testN_data ] = libsvmread(testNName);
%this result is equivalent to specificity.
[predict, accuracy, dec] = svmpredict(testN_label, testN_data, model);


fprintf('\n ----Loading and testing the W data set----\n');
%this result is equivalent to sensitivity
[testW_label,testW_data ] = libsvmread(testWName);
[predict, accuracy, dec] = svmpredict(testW_label, testW_data, model);

